import Vue from 'vue'
import moment from 'moment'

Vue.filter('currency', function (value) {
    if (typeof value === 'string') {
        value = parseInt(value)
    }
    value = value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')
    return '$' + value
})

Vue.filter('date', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY HH:mm')
    }
})

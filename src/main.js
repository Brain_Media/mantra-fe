// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

window.moment = require('moment')
window.$ = window.jQuery = require('jquery')

import Vue from 'vue'
import VeeValidate from 'vee-validate'
import App from './App'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'

require('./filters')
import toastr from './plugins/toastr'

Vue.use(VeeValidate)
Vue.use(VueResource)
Vue.use(toastr)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
})

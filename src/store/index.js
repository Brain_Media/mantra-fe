import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
    strict: debug,

    state: {
        user: {},
        business: {},
        tickets: [],
        projects: [],
        bills: []
    },

    mutations: {
        updateUser (state, user) {
            state.user = {}
            Object.assign(state.user, user)
        },

        updateProjects (state, projects) {
            state.projects = projects
        },

        updateTickets (state, tickets) {
            state.tickets = tickets
        },

        ADD_TICKET (state, ticket) {
            state.tickets.unshift(ticket)
        },

        UPDATE_BILLS (state, bills) {
            state.bills = bills
        }
    }
})

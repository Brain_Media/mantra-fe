import axios from 'axios'
import env from '../../env'
import auth from './auth'

axios.defaults.baseURL = env.BASEURL
axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem(env.TOKEN)

// Intercept all 401
axios.interceptors.response.use((response) => {
    return response
}, (error) => {
    if (error.response.status === 401) {
        console.log('401 intercepted')
        auth.logout()
    }
})

export default {
    get (url) {
        return axios.get(url)
    },

    post (url, request) {
        return axios.post(url, request)
    },

    update (url, request) {
        return axios.put(url, request)
    },

    delete (url) {
        return axios.delete(url)
    },

    setHeaders (token) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    },

    clearToken () {
        axios.defaults.headers.common['Authorization'] = ''
    }
}

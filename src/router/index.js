import Vue from 'vue'
import Router from 'vue-router'
import Guard from '../services/middleware'

// guest pages
import NotFoundPage from '@/views/pages/not-found'
import LoginPage from '@/views/auth/login'
import RegisterOnePage from '@/views/auth/register-one'
import RegisterTwoPage from '@/views/auth/register-two'
import PasswordResetPage from '@/views/auth/password'

// auth pages
import HomePage from '@/views/pages/home'
import AccountPage from '@/views/pages/account'
import BusinessPage from '@/views/pages/business'
import ProjectPage from '@/views/pages/project'
import TicketPage from '@/views/pages/ticket'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', name: 'home', component: HomePage, beforeEnter: Guard.auth },
        { path: '/login', name: 'login', component: LoginPage, beforeEnter: Guard.guest },
        { path: '/register', name: 'register-one', component: RegisterOnePage, beforeEnter: Guard.guest },
        { path: '/register/business', name: 'register-two', component: RegisterTwoPage, beforeEnter: Guard.guest },
        { path: '/password/reset', name: 'password-reset', component: PasswordResetPage, beforeEnter: Guard.auth },
        { path: '/tickets/:id', name: 'ticket', component: TicketPage, beforeEnter: Guard.auth },
        { path: '/projects/:id', name: 'project', component: ProjectPage, beforeEnter: Guard.auth },
        { path: '/bills/:id', name: 'bill', component: PasswordResetPage, beforeEnter: Guard.auth },
        { path: '/account', name: 'account', component: AccountPage, beforeEnter: Guard.auth },
        { path: '/business', name: 'business', component: BusinessPage, beforeEnter: Guard.auth },
        { path: '*', component: NotFoundPage }
    ]
})
